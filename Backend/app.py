from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///goals.db'
db = SQLAlchemy(app)
ma = Marshmallow(app)

class Goal(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(200))
    time_estimate = db.Column(db.Integer, nullable=False)
    priority = db.Column(db.Integer, nullable=False)
    status = db.Column(db.String(50), default='Not started')
    start_time = db.Column(db.Integer, nullable=False)

    def __init__(self, title, description, time_estimate, priority, start_time):
        self.title = title
        self.description = description
        self.time_estimate = time_estimate
        self.priority = priority
        self.start_time = start_time

class GoalSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Goal

goal_schema = GoalSchema()
goals_schema = GoalSchema(many=True)

@app.before_first_request
def create_tables():
    db.create_all()

@app.route('/goals', methods=['GET'])
def get_goals():
    all_goals = Goal.query.order_by(Goal.priority).all()
    return goals_schema.jsonify(all_goals)

@app.route('/goals', methods=['POST'])
def add_goal():
    title = request.json['title']
    description = request.json['description']
    time_estimate = request.json['time_estimate']
    priority = request.json['priority']
    start_time = request.json['start_time']

    new_goal = Goal(title, description, time_estimate, priority, start_time)
    db.session.add(new_goal)
    db.session.commit()
    return goal_schema.jsonify(new_goal)

@app.route('/goals/<id>', methods=['DELETE'])
def delete_goal(id):
    goal = Goal.query.get(id)
    db.session.delete(goal)
    db.session.commit()
    return goal_schema.jsonify(goal)

@app.route('/goals/<id>/status', methods=['PUT'])
def update_status(id):
    goal = Goal.query.get(id)
    status = request.json['status']
    goal.status = status
    db.session.commit()
    return goal_schema.jsonify(goal)

if __name__ == '__main__':
    app.run(debug=True)
