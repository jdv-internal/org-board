import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './App.css';

function App() {
    const [goals, setGoals] = useState([]);
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [timeEstimate, setTimeEstimate] = useState('');
    const [priority, setPriority] = useState('');
    const [startTime, setStartTime] = useState('');

    useEffect(() => {
        fetchGoals();
    }, []);

    const fetchGoals = async () => {
        const response = await axios.get('http://localhost:5000/goals');
        setGoals(response.data);
    };

    const addGoal = async () => {
        const newGoal = {
            title,
            description,
            time_estimate: parseInt(timeEstimate),
            priority: parseInt(priority),
            start_time: parseInt(startTime),
        };
        await axios.post('http://localhost:5000/goals', newGoal);
        setTitle('');
        setDescription('');
        setTimeEstimate('');
        setPriority('');
        setStartTime('');
        fetchGoals();
    };

    const deleteGoal = async (id) => {
        await axios.delete(`http://localhost:5000/goals/${id}`);
        fetchGoals();
    };

    const updateStatus = async (id, status) => {
        await axios.put(`http://localhost:5000/goals/${id}/status`, { status });
        fetchGoals();
    };

    return (
        <div className="container">
            <div className="sidebar">
                <h2>Agenda</h2>
                <div className="agenda" id="agenda">
                    {goals.map((goal, index) => (
                        <div key={index} className="agenda-item">
                            <strong>{goal.title}</strong> - {goal.description}
                            <div>{goal.start_time} - {goal.start_time + goal.time_estimate} minutes</div>
                            <select value={goal.status} onChange={(e) => updateStatus(goal.id, e.target.value)}>
                                <option value="Not started">Not started</option>
                                <option value="In progress">In progress</option>
                                <option value="On hold">On hold</option>
                                <option value="Complete">Complete</option>
                            </select>
                        </div>
                    ))}
                </div>
            </div>
            <div className="main-content">
                <h1>Goal Tracker</h1>
                <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} placeholder="Title of task" />
                <textarea value={description} onChange={(e) => setDescription(e.target.value)} placeholder="Description"></textarea>
                <input type="number" value={timeEstimate} onChange={(e) => setTimeEstimate(e.target.value)} placeholder="Time Estimate (minutes)" />
                <input type="number" value={priority} onChange={(e) => setPriority(e.target.value)} placeholder="Priority (lower is higher)" />
                <input type="number" value={startTime} onChange={(e) => setStartTime(e.target.value)} placeholder="Start Time (minutes from midnight)" />
                <button onClick={addGoal}>Add Goal</button>
                <div id="goals">
                    {goals.map((goal, index) => (
                        <div key={index} className="goal">
                            <strong>{goal.title}</strong> - {goal.description} ({goal.time_estimate} minutes, Priority: {goal.priority}, Status: {goal.status})
                            <button onClick={() => deleteGoal(goal.id)}>Delete</button>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
}

export default App;
